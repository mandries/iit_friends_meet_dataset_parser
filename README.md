Parser for the dataset **Friends meet** (dataset created by the IIT of Genoa)  
Author of the parser: **Mihai ANDRIES** (andries@isir.upmc.fr)  
Date: 2016.02.02  
Code used for the paper: [Modeling the dynamics of individual behaviors for group detection in dynamic crowds using low-level features](http://www.isir.upmc.fr/files/2016ACTI3720.pdf) (RO-MAN 2016)

The parser works in 2 steps:
1.  Convert the binary .mat files present in the dataset into files of .csv format.  
To do this, replace the `main_visualizer.m` file in the dataset with the `main_visualizer.m` file provided in `iit_friends_meet_dataset_parser/src`.  
Execute the new `main_visualizer.m` file in the folder of the dataset.  
This will generate the converted CSV files, both for the tracked persons, and for the groups they are in.  
The generated files are put in the same folder as the dataset's `main_visualizer.m`.
It converts files only from the `real` dataset, and not for the `simulated` dataset.  
If you want both, simply replace the path of the `real` dataset with the path of the `simulated` dataset, and rerun the program.
	
2. Use `parser.cpp` to parse the CSV data (generated in the previous step) and to publish it using a ROS publisher.  
	To do this, execute:
```bash
cd <catkin_workspace>
catkin_make
rosrun iit_friends_meet_dataset_parser parser_iit 
```
	
	
* For your convenience, the result of .mat -> .csv conversion (step 1) is given in the `iit_dataset_real_csv` folder.  
* Also for your convenience, the generated rosbags are in the `generated_bags_with_interpolation` folder.  
* The frame rate of the dataset is 30 Hz.
