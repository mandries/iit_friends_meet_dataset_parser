#define _CRT_SECURE_NO_WARNINGS

#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64.h"
#include <math.h> // for arctan and stuff
#include <tf/tf.h>

#include <rosbag/bag.h> // to be able to write directly into a rosbag

#include "pedsim_msgs/TrackedPerson.h"
#include "pedsim_msgs/TrackedPersons.h"
#include "pedsim_msgs/TrackedGroups.h"
#include "pedsim_msgs/TrackedGroup.h"

#include <map>
#include <set>
#include <cstring>
#include <string>
#include <fstream>

#include <stdlib.h> // for using sleep.h
#include <iostream>
#include <sstream>
#include <vector>

// --------------- IIT PARSER ---------------


// Declare using what
using std::cout;
using std::endl;
using std::ifstream;

// Declare the namespaces used
namespace gm = geometry_msgs;

const int MAX_CHARS_PER_LINE = 2048; // the GT data is quite long
const int TOTAL_TRACKED_TARGETS_IN_IIT_DATASET = 15;
//const char* const DELIMITER = ",";
const char DELIMITER = ',';
double DATA_FREQUENCY_HZ = 30; // 30 Hz
//double SIMILARITY_THRESHOLD = 0.5;
//int MAX_TOTAL_GROUPS = 10000;

// Prepare the structure that will hold all the user data
//std::map<double, pedsim_msgs::TrackedPersons> tracked_persons;
//std::map<double, pedsim_msgs::TrackedGroups> tracked_groups;

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

// IIT dataset declarations
void load_dataset_persons_data(
        std::string ground_truth_persons_file
        //std::map<double, pedsim_msgs::TrackedPersons> tracked_persons // map to fill
        );
void load_dataset_group_data(
        int datafileID,
        std::string ground_truth_groups_file
        //std::map<double, pedsim_msgs::TrackedGroups> tracked_groups // map to fill
        );
void publish_data_to_rosbag(rosbag::Bag bag,
                            std::string tracked_persons_topic_name,
                            std::string tracked_groups_topic_name,
                            std::map<double, pedsim_msgs::TrackedPersons> tracked_persons,
                            std::map<double, pedsim_msgs::TrackedGroups> tracked_groups);


std::map<double, pedsim_msgs::TrackedPersons> tracked_persons;
std::map<double, pedsim_msgs::TrackedGroups> tracked_groups;

// Declaration of function headers
void load_ROS_Tracked_Person_msg_into_memory(
        //std::map<double, pedsim_msgs::TrackedPersons> tracked_persons,
        pedsim_msgs::TrackedPerson msg_tracked_person,
        double timestamp);

int main(int argc, char **argv)
{
    // ROS initialisation
    ros::init(argc, argv, "IIT_data_publisher");
    ros::NodeHandle n;

    std::string tracked_groups_topic_name = "/pedsim/tracked_groups";
    std::string tracked_persons_topic_name = "/pedsim/tracked_persons";

    if (ros::ok())
    {
        // (Plan)
        // For each ground truth pair of files (persons, groups)
        // 1. Load the data
        // 2. Publish it
        // 3. Clean the memory for the next pair of data.
        // TODO follow this design

        int dataset_id = 1;
        int total_datasets = 15;

        for (dataset_id = 1; dataset_id <= total_datasets; dataset_id+=1)
        {
            //printf("LOOP\n");
            std::string rosbag_name = "../generated_bags_with_interpolation/iit_dataset_real_";
            std::string datasetIDstring;
            std::stringstream out;
            out << dataset_id;
            datasetIDstring = out.str();
            rosbag_name .append(datasetIDstring.c_str())
                    .append(".bag");

            rosbag::Bag bag;
            cout << "Rosbagname: " << rosbag_name << endl;
            bag.open(rosbag_name, rosbag::bagmode::Write);

            // (Implementation
            // 1. Load the data (TODO)
            // 1a Specify file names
            std::string ground_truth_persons_path = "../iit_dataset_real_csv/GT_with_interpolation/";
            std::string ground_truth_groups_path = "../iit_dataset_real_csv/GTgroup/";
            std::string ground_truth_persons_file =
                    ground_truth_persons_path
                    //.append("GT1_S")
                    .append("GT1_S")
                    .append(datasetIDstring)
                    .append(".csv");
            std::string ground_truth_groups_file =
                    ground_truth_groups_path
                    .append("GTgroup_S")
                    .append(datasetIDstring)
                    .append(".csv");

            load_dataset_persons_data(
                        ground_truth_persons_file
                        //tracked_persons // map to fill
                        );

            // 1b Load the group data after persons (as it is based on person_id from persons data)
            load_dataset_group_data(
                        dataset_id,
                        ground_truth_groups_file
                        //tracked_groups // map to fill
                        );


            cout << "Bag size: " << bag.getSize() << endl;

            cout << "Tracked groups map size: " << tracked_groups.size() << endl;
            cout << "Tracked persons map size: " << tracked_persons.size() << endl;


            // 2. Publish data to rosbag
            double max_time = std::max(tracked_persons.size(), tracked_groups.size()) / DATA_FREQUENCY_HZ;
            double time_counter;
            double time_counter_step = 1.0/DATA_FREQUENCY_HZ; // 30 Hz for the IIT dataset
            for (time_counter = 1.0;
                 time_counter < max_time;
                 time_counter += time_counter_step)
                //for(std::map<double, pedsim_msgs::TrackedPersons>::iterator iter_persons = tracked_persons.begin();
                //    iter_persons != tracked_persons.end();
                //    ++iter_persons)
            {
                if (    (tracked_persons.find(time_counter) != tracked_persons.end()) &&
                        (tracked_groups.find(time_counter) != tracked_groups.end())
                        )
                {
                    // Publish the tracked persons data
                    bag.write(tracked_persons_topic_name,
                              tracked_persons.find(time_counter)->second.header.stamp,
                              tracked_persons.find(time_counter)->second);

                    // Publish the tracked groups data
                    bag.write(tracked_groups_topic_name,
                              tracked_groups.find(time_counter)->second.header.stamp,
                              tracked_groups.find(time_counter)->second
                              );
                }
            }
            /*
            publish_data_to_rosbag(
                    bag,
                    tracked_persons_topic_name,
                    tracked_groups_topic_name,
                    tracked_persons,
                    tracked_groups);
                    //loop_rate);
            */

            printf("Cleaning memory\n");

            // 3. Clean the memory.
            tracked_persons.clear();
            tracked_groups.clear();

            printf("Cleaning memory done.\n");
            printf("Bag closing...\n");

            //sleep(2);
            cout << "Bag size: " << bag.getSize() << endl;
            // Close the bag when finished writing to it.
            bag.close();

            printf("Bag closed\n");
        }
    } // end of if ROS OK
}


/** Parses the tracking data for persons, and loads it into memory */
void load_dataset_group_data(
        int datasetID,
        std::string ground_truth_groups_file
        //        std::map<double, pedsim_msgs::TrackedGroups> tracked_groups // map to fill
        )
{
    ifstream fin_groups(ground_truth_groups_file.c_str());
    if (!fin_groups.is_open())
    {
        cout << "Failed to open file " << ground_truth_groups_file << endl;
        return;
    }
    else
    {
        cout << "Success in opening the Tracked_Groups file: " << ground_truth_groups_file << endl;
    }

    // Parsing plan:
    //  1. Parse the group data, and load it into memory as a ROS message
    // Done

    // Skip the first line of the file (contains the titles of columns)
    std::string readLine;
    std::getline(fin_groups, readLine);

    //printf("First line skipped\n");

    // Time starts at 1.0
    double groups_time_since_recording_start_in_units=1.0;

    // Read each line of the file
    while (!fin_groups.eof())
    {
        // read an entire line into memory
        // std::string readLine; //already declared
        std::getline(fin_groups, readLine);

        // vector to store memory addresses of the tokens in buf
        int MAX_LINE_SIZE = TOTAL_TRACKED_TARGETS_IN_IIT_DATASET;
        std::vector<std::string> token_groups (MAX_LINE_SIZE);
        token_groups = split(readLine, DELIMITER);

        // Parse the line
        // Format of the line:
        // GroupID (user 0), GroupID (user 1), GroupID (user 2), ...
        // * no timestamp, it's implicit
        // * Time starts at different times in the different files (units = ms?)

        if (token_groups.size() > 0) // non empty line
        {
            // Header              header      # Header containing timestamp etc. of this message
            // TrackedGroup[]      groups      # All groups that are currently being tracked

            // Make a new tracked_groups entry, and put the info inside it (in the called function)
            pedsim_msgs::TrackedGroups msg_tracked_groups;
            ros::Time timestamp_ros (groups_time_since_recording_start_in_units);
            msg_tracked_groups.header.stamp = timestamp_ros;

            // Multiple users can be assigned to the same group.
            // Use such a structure, that fits this format (map, or set)
            std::map<int, pedsim_msgs::TrackedGroup> tracked_groups_map;

            // The counter corresponds to the UserID.
            int tokenID;
            for (tokenID=0; tokenID < token_groups.size(); tokenID+=1)
            {
                // The columns do not correspond to user user IDs...
                // Look in tracked_users, to find the real ID of the user in column id_5
                // Something like: tracked_users.find(timestamp).tracks[tokenID].userID
                //cout << "group timestamp: " << groups_time_since_recording_start_in_units << " s; tokenID: " << tokenID << endl;

                // Check if an entry of tracked persons exists for this timestamp
                if (tracked_persons.find(groups_time_since_recording_start_in_units) != tracked_persons.end())
                {
                    // Check if groupID not empty for this user.
                    if (!token_groups[tokenID].empty())
                    {
                        // 1. Add group to tracked groups (if absent)
                        // 2. Add user to the new group, or to the existing one

                        // TODO check if there is tracking data in this timestamp
                        //cout << "size of tracked_persons[] at time " << groups_time_since_recording_start_in_units << ": " << tracked_persons.find(groups_time_since_recording_start_in_units)->second.tracks.size() << endl;

                        // Check if the entry of tracked persons for this timestamp is not empty.
                        if (tracked_persons.find(groups_time_since_recording_start_in_units)->second.tracks.size() > 0)
                        {

                            int user_id =  tracked_persons.find(groups_time_since_recording_start_in_units)->second.tracks[tokenID].track_id;

                            // Get the group ID
                            int user_group_id = std::atoi(token_groups[tokenID].c_str());

                            // Users not assigned to a group are given the groupID=0;
                            // Avoid processing this case (as demanded by OMAR)
                            if (user_group_id > 0)
                            {
                                user_group_id += 1; // WARNING: THIS IS A REQUEST BY OMAR, TO AVOID USING GROUP_ID = 0

                                // If no such group is known, add it
                                // Else do nothing
                                if (tracked_groups_map.find(user_group_id) == tracked_groups_map.end())
                                {
                                    pedsim_msgs::TrackedGroup msg_tracked_group;
                                    msg_tracked_group.group_id = user_group_id; // TODO maybe you need to convert from int to uint64 here.
                                    tracked_groups_map.insert(std::pair<int, pedsim_msgs::TrackedGroup>(user_group_id, msg_tracked_group));
                                }

                                // In both cases, find this instance of tracked group, and add the ID of the person assigned to it.
                                tracked_groups_map.find(user_group_id)->second.track_ids.push_back(user_id);
                            }
                        }
                    }
                }
            }

            // The set with IDs of groups is now filled
            // Loop through the map, to add the TrackedGroup structures to the msg_tracked_groups.
            for(std::map<int, pedsim_msgs::TrackedGroup>::iterator iter_group = tracked_groups_map.begin();
                iter_group != tracked_groups_map.end();
                ++iter_group)
            {
                // REQUEST BY OMAR: Declare a group only when there's more than one person in it.
                if (iter_group->second.track_ids.size() > 1)
                {
                    msg_tracked_groups.groups.push_back(iter_group->second);
                }
            }

            //cout << "Tracked groups size before insertion: " << tracked_groups.size() << endl;
            // Put the container into memory
            tracked_groups.insert(std::pair<double, pedsim_msgs::TrackedGroups>(
                                      groups_time_since_recording_start_in_units,
                                      msg_tracked_groups));

            //cout << "Tracked groups size after insertion: " << tracked_groups.size() << endl;
        } // if Line not empty

        // Increase time counter by the preset (1 time units per line)
        groups_time_since_recording_start_in_units += (1.0 / DATA_FREQUENCY_HZ);
    } // end of while not file end
}



/** Parses the data for the tracked groups and loads it into memory*/
void load_dataset_persons_data(std::string ground_truth_persons_file)
{
    ifstream fin_tracked_persons(ground_truth_persons_file.c_str());
    if (!fin_tracked_persons.is_open())
    {
        cout << "Failed to open file " << ground_truth_persons_file << endl;
        return;
    }
    else
    {
        cout << "Success in opening Geometry file" << endl;
    }

    double persons_time_since_recording_start_in_units = 1.0;

    // Skip first line with the names of columns
    std::string firstLine;
    std::getline(fin_tracked_persons, firstLine);

    int COLUMNS_PER_USER_IN_GT_data = 9;
    const int MAX_TOKENS_PER_GT_PERSON_LINE = TOTAL_TRACKED_TARGETS_IN_IIT_DATASET * COLUMNS_PER_USER_IN_GT_data;

    // vector to store memory addresses of the tokens in buf
    std::vector<std::string> token_person_tracking_data_header(MAX_TOKENS_PER_GT_PERSON_LINE);

    token_person_tracking_data_header = split(firstLine, DELIMITER);
    int total_users_header = token_person_tracking_data_header.size()/COLUMNS_PER_USER_IN_GT_data;
    ros::Time timestamp_ros (persons_time_since_recording_start_in_units);
    cout << ground_truth_persons_file << " : total users : " << total_users_header << endl;


    // Parsing plan:
    //  1. Read the geometry data (a line for every 3 seconds)
    //  2. For each geometry data line, get the acceleration data line (a line for every 0.05 seconds)
    //  3. Mix the two into a single ROS message
    //  4. Put this ROS message into memory

    // read each line of the file
    while (!fin_tracked_persons.eof())
    {
        // read an entire line into memory
        //char buf[MAX_CHARS_PER_LINE];
        std::string readLine;
        std::getline(fin_tracked_persons, readLine);


        // vector to store memory addresses of the tokens in buf
        std::vector<std::string> token_person_tracking_data(MAX_TOKENS_PER_GT_PERSON_LINE);

        // parse the line
        token_person_tracking_data = split(readLine, DELIMITER);

        //cout << ground_truth_persons_file << " time " << persons_time_since_recording_start_in_units << " tokens: " << token_person_tracking_data.size(); // << endl;

        //int total_users = token_person_tracking_data.size()/COLUMNS_PER_USER_IN_GT_data;
        ros::Time timestamp_ros (persons_time_since_recording_start_in_units);

        //  According to the documentation, pos_ind a structure GT(t) that contains:
        //	X the positions of each individual in the image,
        //	V its velocity,
        //	id the identifier,
        //	Xw the ground floor position (in meters) and
        //	Vw its ground floor velocity.
        //
        // But in fact, it contains:
        // 1. timestamp
        // 2. user ID
        // 3. position X
        // 4. position Y
        // 5. speed X
        // 6 . speed Y
        if (token_person_tracking_data.size() > 0)
        {
            //# Message defining a tracked person
            //uint64      track_id        # unique identifier of the target, consistent over time
            //bool        is_occluded     # if the track is currently not observable in a physical way
            //bool        is_matched      # if the track is currently matched by a detection
            //uint64      detection_id    # id of the corresponding detection in the current cycle (undefined if occluded)
            //duration    age             # age of the track
            //# The following fields are extracted from the Kalman state x and its covariance C
            //geometry_msgs/PoseWithCovariance       pose   # pose of the track (z value and orientation might not be set, check if corresponding variance on diagonal is > 99999)
            //geometry_msgs/TwistWithCovariance   twist     # velocity of the track (z value and rotational velocities might not be set, check if corresponding variance on diagonal is > 99999)

            // Load the parsed TrackedPerson data into memory, as a ROS message
            // If no such element found, make the englobing structure and add it to the map.
            if (tracked_persons.find(persons_time_since_recording_start_in_units) == tracked_persons.end())
            {
                pedsim_msgs::TrackedPersons msg_tracked_persons;
                ros::Time timestamp_ros(persons_time_since_recording_start_in_units);
                msg_tracked_persons.header.stamp = timestamp_ros;

                // Generate the parent structure
                tracked_persons.insert(std::pair<double, pedsim_msgs::TrackedPersons>(persons_time_since_recording_start_in_units, msg_tracked_persons));
            }

            // Generate data-containers for the users
            int user_counter;
            for (user_counter=0;
                 user_counter < total_users_header;
                 user_counter++)
            {
                // Parse the data for the users
                // 1. x Parse the positions (x,y)
                // 2. x Parse the velocities (x,y)
                // 3. Parse the identifiers (id)
                // 4. Parse the ground floor position (in meters: x,y)
                // 5. Parse the ground floor velocity (x,y)

                pedsim_msgs::TrackedPerson msg_tracked_person;
                //ros::Time timestamp_ros(persons_time_since_recording_start_in_units);
                //msg_tracked_person.header.stamp = timestamp_ros; // set the time

                // Add data only about humans present in the scene
                if (!token_person_tracking_data[total_users_header * (2 + 2) + user_counter].empty())
                {
                    msg_tracked_person.track_id = std::atoi(token_person_tracking_data[total_users_header * (2 + 2) + user_counter].c_str()); //2 + 2 = positions (x,y), velocities (x.y)
                    msg_tracked_person.pose.pose.position.x = std::atof(token_person_tracking_data[total_users_header * (2 + 2 + 1) + (user_counter * 2)].c_str());
                    msg_tracked_person.pose.pose.position.y = std::atof(token_person_tracking_data[total_users_header * (2 + 2 + 1) + (user_counter * 2) + 1].c_str());
                    msg_tracked_person.twist.twist.linear.x = std::atof(token_person_tracking_data[total_users_header * (2 + 2 + 1 + 2) + (user_counter * 2)].c_str());
                    msg_tracked_person.twist.twist.linear.y = std::atof(token_person_tracking_data[total_users_header * (2 + 2 + 1 + 2) + (user_counter * 2) + 1].c_str());
                    msg_tracked_person.twist.twist.linear.z = 0;

                    double velocity_x = msg_tracked_person.twist.twist.linear.x;
                    double velocity_y = msg_tracked_person.twist.twist.linear.y;
                    double angle_in_rad = std::atan2(velocity_y, velocity_x); // y,x

                    // Calculate the corresponding quaternion
                    tf::Quaternion q_tf;
                    q_tf = tf::createQuaternionFromRPY(
                                0,
                                0,
                                angle_in_rad);
                    q_tf.normalize(); // Normalize the quaternion Such that x^2 + y^2 + z^2 +w^2 = 1
                    gm::Quaternion q_msg; // The quaternion container in MSG format
                    tf::quaternionTFToMsg(q_tf, q_msg); // Cast the quaternion into MSG format
                    msg_tracked_person.pose.pose.orientation = q_msg; // Set the quaternion value inside the message

                    // Add the tracked person to memory
                    tracked_persons.find(persons_time_since_recording_start_in_units)->second.tracks.push_back(msg_tracked_person);
                }
            }

            //cout << " tracked persons: " << tracked_persons.find(persons_time_since_recording_start_in_units)->second.tracks.size() << endl;
        } // if Line not empty

        // Increase the time counter after each read line.
        persons_time_since_recording_start_in_units += (1.0 / DATA_FREQUENCY_HZ);
    } // end of while not file end
}


/** Loads the given ROS TrackedPerson message into memory.
    Create the TrackedPersons message for the given timestamp, if necessary. */
void load_ROS_Tracked_Person_msg_into_memory(
        //std::map<double, pedsim_msgs::TrackedPersons> tracked_persons, // map to fill
        pedsim_msgs::TrackedPerson msg_tracked_person,
        double timestamp
        )
{
    // If no such element found, make the englobing structure and add it to the map.
    if (tracked_persons.find(timestamp) == tracked_persons.end())
    {
        pedsim_msgs::TrackedPersons msg_tracked_persons;

        // Casting the time into time_t format
        //time_t timestamp_time_t = timestamp;
        ros::Time timestamp_ros(timestamp);
        msg_tracked_persons.header.stamp = timestamp_ros;
        //msg_tracked_persons.tracks.push_back(msg_tracked_person);

        // Generate the parent structure
        tracked_persons.insert(std::pair<double, pedsim_msgs::TrackedPersons>(timestamp, msg_tracked_persons));
    }
    // If the element was found (the corresponding ROS message with TrackerPersons,
    //  put inside it our newly TrackedPerson.

    // pedsim_msgs::TrackedPersons msg_tracked_persons = tracked_persons.find(timestamp)->second;
    // msg_tracked_persons.tracks.push_back(msg_tracked_person);
    tracked_persons.find(timestamp)->second.tracks.push_back(msg_tracked_person);
}




// Requires
// #include <string>
// #include <sstream>
// #include <vector>
/** Tokenize a string and put the tokens into the given "elemes" vector.*/
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}
/** Tokenize a string and generate a new vector with the tokenized elements.*/
std::vector<std::string> split(const std::string &s, char delim)
{
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
