clear; clc; close all;
addpath('my_utils/')

%% --- user choice (not parameters) --- %%
dataset_path = ['./FM_dataset'];
res_path     = 'RES';

%% --- Dataset Selection --- %%
dataset_sel_GUI;


%% --- SEQUENCE START --- %%
fprintf('=> Sequance started! \n');
for t = 1:last_frame
 
    if d_chosen==1 % synth
     
        img = uint8(squeeze(video(t,:,:,:)));
        display_GT_synth;
        pause(0.02);
        
    elseif d_chosen==2 % real
        
        img  = read(readerobj,[t t]);
        display_GT_real;
        drawnow; %pause(0.01);
    end
    
end
