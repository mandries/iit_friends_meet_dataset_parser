d_chosen = menu('Choose the dataset','FM synth','FM real');
trick_eps= 0.001;
switch d_chosen
    
%% Friend Meet synth
    case 1
        pathvideo = [dataset_path '/synth/'];
        
        d = dir([pathvideo '*.mat']);
        n_dat = menu('Choose the sequence',d.name);
        
        calibvideo  = pathvideo;
        aviname     = d(n_dat).name;      
        
        % load ground truth
        try
            load([pathvideo strtok(aviname,'.') '.mat']);
            load([calibvideo '/ground_truth/GTgroups_' strtok(aviname,'.') '.mat']);
        catch
            fprintf('GT not found!')
        end
        
        box_par.DIMX = 16; box_par.DIMY = 16; % visualization sx
        box_par.DIMXX = box_par.DIMX;    box_par.DIMYY = box_par.DIMY;
        box_par.DONWY = 0;
        
        last_frame = length(GTs);
        
%% Friend Meet real
    case 2
        pathvideo = [dataset_path '/real/'];
        
        d = dir([pathvideo 'S*.avi']);
        n_dat = menu('Choose the sequence',d.name);

        calibvideo  = pathvideo;
        aviname     = d(n_dat).name;
        pathvideo   = [pathvideo aviname];
        
        % homography parameters loading
        H = textread([calibvideo 'H.txt']);
        H = H^(-1);
        
        readerobj = VideoReader(pathvideo); % avi handler
        
        % load ground truth
        try
            load([calibvideo '/ground_truth/GT_' strtok(aviname,'.') '.mat']);
            load([calibvideo '/ground_truth/GTgroup_' strtok(aviname,'.') '.mat']);

        labels  = single(unique(pos_ind(:,2)));
        
        % MIHAI: this is useless code, make the program generate incorrect data
        % project to image plane
        % x = H^(-1)*[pos_ind(:,3:4),ones(size(pos_ind,1),1)]';
        % x = x./repmat(x(3,:),size(x,1),1); % normalize
        % pos_ind(:,3:4) = x(1:2,:)';
        
        A2  = [];
        for label = labels'
            ind_curr_ID = pos_ind(:,2)==label;
            % note: we add noise to avoid to have equal points in x and y
            A1 = [pos_ind(ind_curr_ID,3)+rand(sum(ind_curr_ID),1)*trick_eps,...
                pos_ind(ind_curr_ID,4)+rand(sum(ind_curr_ID),1)*trick_eps,...
                pos_ind(ind_curr_ID,1)];
            
            if size(A1)>1
                xx = []; yy = []; tt = [];
                for i=2:size(A1,1)
                    npt= (A1(i,3)-A1(i-1,3))+1;
                    x = linspace( A1(i-1,1),A1(i,1),npt)';
                    y = interp1(A1(i-1:i,1),A1(i-1:i,2),x,'linear');
                    t = [A1(i-1,3):A1(i,3)-1]';
                    x = x(1:end-1);
                    y = y(1:end-1);
                    xx = [xx;x];
                    yy = [yy;y];
                    tt = [tt;t];
                end
                xx = [xx;A1(end,1)];
                yy = [yy;A1(end,2)];
                tt = [tt;A1(end,3)];
                
                % compute velocities (dt = 1 frame)
                v_x = []; v_y = [];
                for i = 2:length(xx)
                    v_x(i) = xx(i)-xx(i-1);
                    v_y(i) = yy(i)-yy(i-1);
                end
                v_x(1) = v_x(2);  v_y(1) = v_y(2);
            else
                xx = A1(1,1);   yy = A1(1,2);   tt = A1(1,3);
                v_x = 0;    v_y = 0;
            end
            A2 = [A2;tt+1,repmat(label,length(tt),1),yy,xx,v_y',v_x'];
        end
        A2 = double(A2);
        [~,inds]    = sort(A2(:,1)); % sort by frame num (as ETH dataset)
        pos_ind     = A2(inds,:);
        very_first_frame= A2(1,1);
        last_frame      = A2(end,1);
        clear A2 xx yy A1
        catch
            fprintf('GT not found!')
        end
        
        % ROI
        box_par.DIMX = 40; box_par.DIMY = 50; % visualization sx
        box_par.BOX_OF_INTEREST = [1,1,960,720];
        box_par.DIMXX = box_par.DIMX;    box_par.DIMYY = box_par.DIMY+50;
        box_par.DONWY = 0;
    
        % Retrieve Ground truth structure
        fprintf('=> Load the ground truth...');
        GT = struct([]);
        S = 4;      % x state space size (pos,vel)
        last_frame  = readerobj.NumberOfFrames;
        for t = 1:last_frame
            curr_t_gt = pos_ind(:,1) == t;
            GT(t).X = [pos_ind(curr_t_gt,4)';pos_ind(curr_t_gt,3)']; % [x,y]
            GT(t).V = [pos_ind(curr_t_gt,6)';pos_ind(curr_t_gt,5)']; % [vx,vy]
            GT(t).id = pos_ind(curr_t_gt,2)';
            % map image to world
            [GT(t).Xw,GT(t).Vw] = img2floor(GT(t).X,GT(t).V,H);
        end
        fprintf(' OK \n')
        invH    = inv(H); % precompute it (more efficient)

        % Mihai extract data
        %writetable(struct2table(GT), 'extracted_data/GT/GT1_S15.csv');
        %fprintf('export OK \n')
        
    otherwise
        error('Dataset NOT selected')
end

Tcolor  = rand(100,3);
Gcolor  = rand(100,3);

% plot
scrsz   = get(0,'ScreenSize');    % display size
h_seq   = figure; set(gcf,'Position',[1 1 scrsz(3)/2 scrsz(4)/2]);
fprintf('%s video Selected \n',aviname);

